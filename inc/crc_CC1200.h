/*
 * crc_CC1200.h
 *
 *  Created on: Aug 17, 2018
 *      Author: Tomas Jakubik
 */

#ifndef CRC_CC1200_H_
#define CRC_CC1200_H_

#include <stdint.h>

#define CRC_CC1200_INIT_VALUE   0xffff	///< CRC init vector

/**
 * @brief CRC one byte of data.
 * @param last_crc previous crc value
 * @param data byte to crc
 * @return new crc value
 */
uint16_t crc_CC1200_byte(uint16_t last_crc, uint8_t data);

/**
 * @brief CRC array of bytes
 * @param data byte data
 * @param len number of bytes in data
 * @return crc value
 */
uint16_t crc_CC1200_data(uint8_t* data, uint32_t len);

#endif /* CRC_CC1200_H_ */
