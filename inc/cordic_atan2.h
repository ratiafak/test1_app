/*
 * cordic_atan2.c
 *
 *  Created on: November 2017
 *      Author: ST microelectronics DT0087
 *  Modified on: Aug 24, 2018
 *      Author: Tomas Jakubik
 *
 *  This module calculates atan2 using CORDIC algorithm.
 */

#ifndef CORDIC_ATAN2_H_
#define CORDIC_ATAN2_H_

//CORDIC, 16 bits, 14 iterations
// 1.0 = 8192.000000 multiplication factor
// A   = 1.743165 convergence angle (limit is 1.7432866 = 99.9deg)
// F   = 1.646760 gain (limit is 1.64676025812107)
// 1/F = 0.607253 inverse gain (limit is 0.607252935008881)
// pi = 3.141593 (3.1415926536897932384626)
#define  CORDIC_A           1.743165 // CORDIC convergence angle A
#define  CORDIC_F           0x000034B2 // CORDIC gain F
#define  CORDIC_1F          0x0000136F // CORDIC inverse gain 1/F
#define  CORDIC_HALFPI      0x00003244
#define  CORDIC_PI          0x00006488
#define  CORDIC_TWOPI       0x0000C910
#define  CORDIC_MUL         8192.000000 // CORDIC multiplication factor M = 2^13
#define  CORDIC_MAXITER     14

/**
 * @brief Calculate atan2.
 * @param y
 * @param x
 * @return angle radians multiplied by CORDIC multiplication factor M
 */
int cordic_atan2(int y, int x);

#endif /* CORDIC_ATAN2_H_ */
