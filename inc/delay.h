/*
 * delay.h
 *
 *  Created on: Apr 19, 2018
 *      Author: Tomas Jakubik
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <stdint.h>

/**
 * @brief MCU delay.
 * @param time time [us]
 */
void delay_us(uint32_t time);

/**
 * @brief Replacement for system call.
 * @param from time [us]
 * @param to time [us]
 */
void usleep_range(uint32_t from, uint32_t to);

#endif /* DELAY_H_ */
