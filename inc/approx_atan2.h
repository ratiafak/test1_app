/*
 * approx_atan2.h
 *
 *  Created on: Jul 13, 2018
 *      Author: Tomas Jakubik
 */

#ifndef APPROX_ATAN2_H_
#define APPROX_ATAN2_H_

#include <stdint.h>

/**
 * @brief Approximate atan2 from complex number.
 * @note For one octant: x = (smaller << 16)/bigger; angle [-pi=int16_min pi=int16_max] = (x*x*-2826 + x*724190871) >> 32;
 * @param real real part of the complex number
 * @param imag imaginary part of the complex number
 * @return angle in full range of int16_t [-pi=int16_min pi=int16_max]
 */
int16_t approx_atan2(int16_t real, int16_t imag);

#endif /* APPROX_ATAN2_H_ */
