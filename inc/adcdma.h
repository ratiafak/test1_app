/*
 * adcdma.h
 *
 *  Created on: Apr 12, 2018
 *      Author: Tomas Jakubik
 */

#ifndef ADCDMA_H_
#define ADCDMA_H_

#include "LPC43xx.h"
#include "intercom.h"

#define ADCDMA_TRANSFER_SIZE   INTERCOM_BUFFER_SIZE	//Number of uint32_ts (x2 samples if packed)

void adcdma_init(uint32_t* buffer0, uint32_t* buffer1);

#endif /* ADCDMA_H_ */
