/*
 * adcdma.c
 *
 *  Created on: Apr 12, 2018
 *      Author: Tomas Jakubik
 */

#include "adcdma.h"

typedef struct adcdma_lli_s
{
	void* source;	///< Source address
	void* destination;	///< Destination address
	struct adcdma_lli_s* next;	///< Next lli address
	uint32_t control;	///< DMA channel control register
} adcdma_lli_t;

adcdma_lli_t adcdma_lli[2];	///< Linked list for pingpong buffer

/**
 * @brief Initialize DMA and its clock for ADCHS input.
 * @param buffer0 address of the first ADCDMA_TRANSFER_SIZE sample buffer
 * @param buffer1 address of the second ADCDMA_TRANSFER_SIZE sample buffer
 */
void adcdma_init(uint32_t* buffer0, uint32_t* buffer1)
{
	LPC_CCU1->CLK_M4_DMA_CFG = 0x1;	//Enable DMA clock
	LPC_GPDMA->CONFIG = 0x1;	//Enable DMA
	LPC_GPDMA->C0CONFIG &= ~0x1;	//Disable channel 0

	//Set up linked list
	adcdma_lli[0].source = (void*)&(LPC_ADCHS->FIFO_OUTPUT);
	adcdma_lli[0].destination = buffer0;
	adcdma_lli[0].next = &adcdma_lli[1];
	adcdma_lli[0].control = (1 << 31) | (1 << 27) | (1 << 25) | (1 << 24)	//Terminal count interrupt, dst increment, src master 1, dst master 1
			| (0x2 << 21) | (0x2 << 18) | (0x1 << 15) | (0x1 << 12) | ADCDMA_TRANSFER_SIZE;	//32 bit, 4 burst (8 samples), transfer size
	adcdma_lli[1].source = (void*)&(LPC_ADCHS->FIFO_OUTPUT);
	adcdma_lli[1].destination = buffer1;
	adcdma_lli[1].next = &adcdma_lli[0];
	adcdma_lli[1].control = (1 << 31) | (1 << 27) | (1 << 25) | (1 << 24)	//Terminal count interrupt, dst increment, src master 1, dst master 1
			| (0x2 << 21) | (0x2 << 18) | (0x1 << 15) | (0x1 << 12) | ADCDMA_TRANSFER_SIZE;	//32 bit, 4 burst (8 samples), transfer size

	intercom.lli0_address = (uint32_t)&adcdma_lli[0];	//Store address of the first link register for M4 core

	//Configure DMA channel0
	LPC_GPDMA->C0CONFIG = (0x1 << 15) | (0x1 << 14) | (0x2 << 11) | (0x8 << 1);	//Error and terminal count interrupts, ADCHS to memory
	LPC_CREG->DMAMUX |= 0x3 << 16;	//Multiplex to ADCHS

	//Load first lli element
	LPC_GPDMA->C0SRCADDR = (uint32_t)adcdma_lli[0].source;
	LPC_GPDMA->C0DESTADDR = (uint32_t)adcdma_lli[0].destination;
	LPC_GPDMA->C0LLI = ((uint32_t)adcdma_lli[0].next) & ~0x3;
	LPC_GPDMA->C0CONTROL = adcdma_lli[0].control;

	//Enable channel 0
	LPC_GPDMA->C0CONFIG |= 0x1;
}
