/*
 * chproc.c
 *
 *  Created on: Aug 17, 2018
 *      Author: Tomas Jakubik
 *  chproc ~ channel processing
 */

#include "chproc.h"
#if CHPROC_ATAN_TYPE == 1	/*Use approx_atan2*/
#include "approx_atan2.h"
#elif CHPROC_ATAN_TYPE == 2	/*Use cordic_atan2*/
#include "cordic_atan2.h"
#endif	/*Which atan2 to use*/
#include "crc_CC1200.h"

chproc_t chproc[CHPROC_N];	///< Chproc structures

#if GLOBAL_SEND_STUFF == 1
int16_t fdev[FDEV_SIZE];
int16_t data[FDEV_SIZE];
#warning "Temporary"
uint32_t fdev_ptr = 0;
uint32_t send = 0;
#endif	/*GLOBAL_SEND_STUFF*/

chproc_glob_t chproc_glob = {
		.atan2_cnt = 0,
};	///< Chproc state variables

/**
 * @brief Advance channel into CHPROC_STATE_MAG state.
 * @param channel
 * @return nonzero on error
 */
uint32_t chproc_impulse_power(uint32_t channel)
{
	uint32_t i;

	for(i = 0; i < CHPROC_N; i++)
	{
		if((chproc[i].channel == channel)
				&& ((chproc[i].state == CHPROC_STATE_MAG)
						|| (chproc[i].state == CHPROC_STATE_PREAM)
						|| (chproc[i].state == CHPROC_STATE_SYNC)))	//Already active
		{
			return 1;	//Ignore
		}
	}

	if(chproc_glob.atan2_cnt > CHPROC_ATAN2_MAX)	//Too many atan2 is being calculated
	{
		return 2;	//No atan2 calculation time left
	}

	for(i = 0; i < CHPROC_N; i++)
	{
		if(chproc[i].state == CHPROC_STATE_IDLE)	//Free chproc
		{
			chproc[i].state = CHPROC_STATE_MAG;	//Start processing this channel
			chproc[i].channel = channel;
			chproc[i].state_symbols = 0;
			chproc[i].packet_symbols = 0;
			chproc[i].dc_remove = 0;
			chproc[i].symbols_pream = 0;
			chproc[i].pream_match_cnt = 0;
			chproc_glob.atan2_cnt++;	//Increment atan2 counter
			return 0;	//Successfully added
		}
	}

	return 3;	//No space left
}

/**
 * @brief Limit and wrap angle.
 * @param angle input
 * @return output
 */
int32_t chproc_wrap(int32_t angle)
{
#if CHPROC_ATAN_TYPE == 2	/*Use cordic_atan2*/
	if(angle > CORDIC_PI)
	{
		return angle - CORDIC_TWOPI;
	}
	else if(angle < (-1*CORDIC_PI))
	{
		return angle + CORDIC_TWOPI;
	}
	else
	{
		return angle;
	}
#endif	/*Use cordic_atan2*/
}

#if CHPROC_ATAN_TYPE == 1	/*Use approx_atan2*/
/**
 * @brief Calculate frequency deviation for one chproc.
 * @param chproc pointer to chproc evaluated
 * @param fft_out complex fft data for all channels
 */
void chproc_fdev(chproc_t* chproc, int16_t* fft_out)
{
	int16_t angle;

	angle = approx_atan2(fft_out[2*chproc->channel], fft_out[2*chproc->channel+1]);	//Calculate atan2 angle
	chproc->fdev[chproc->packet_symbols & CHPROC_FDEV_MASK] = (int16_t)(chproc->last_angle - angle);	//Subtract angles to get frequency deviation
	///@note Fdev is negated, because demodulator flips frequencies.
	chproc->last_angle = angle;	//Remember current angle for the next time
}
#elif CHPROC_ATAN_TYPE == 2	/*Use cordic_atan2*/
/**
 * @brief Calculate frequency deviation for one chproc.
 * @param chproc pointer to chproc evaluated
 * @param fft_out complex fft data for all channels
 */
void chproc_fdev(chproc_t* chproc, int16_t* fft_out)
{
	int32_t angle;
	int32_t diff;

	angle = cordic_atan2(fft_out[2*chproc->channel+1], fft_out[2*chproc->channel]);	//Calculate atan2 angle
	diff = chproc_wrap(chproc->last_angle - angle);	//Subtract angles to get frequency deviation
	///@note Fdev is negated, because demodulator flips frequencies.
	chproc->fdev[chproc->packet_symbols & CHPROC_FDEV_MASK] = (int16_t)(diff);	//Subtract angles to get frequency deviation
	chproc->last_angle = angle;	//Remember current angle for the next time
}
#else	/*Which atan2 to use*/
#error "Select atan2!"
#endif	/*Which atan2 to use*/

/**
 * @brief Process one sample of active channels
 * @param fft_out complex fft data for all channels
 * @return number of active chprocs
 */
uint32_t chproc_tick(int16_t* fft_out)
{
	uint_fast8_t i;
	uint32_t active_cnt = 0;

	for(i = 0; i < CHPROC_N; i++)	//Check every chproc
	{
		if(chproc[i].state != CHPROC_STATE_IDLE)	//Active chproc
		{
			active_cnt++;	//Count active chprocs

			switch(chproc[i].state)
			{
			case CHPROC_STATE_MAG:	//Magnitude rise done, looking for preamble
				chproc_fdev(&chproc[i], fft_out);	//Get fdev
				chproc[i].dc_remove = ((int32_t)chproc[i].fdev[0] + (int32_t)chproc[i].fdev[1]
						+ (int32_t)chproc[i].fdev[2] + (int32_t)chproc[i].fdev[3]) >> 2;	//Average DC component

				chproc[i].symbols_pream <<= 1;	//Shift symbols in time

				if(chproc_wrap(chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] - chproc[i].dc_remove) > 0)	//Symbol is larger than DC component
				{
					chproc[i].symbols_pream |= 0x1;	//Symbol '1'
				}
				if(((chproc[i].symbols_pream & 0x55) == 0x11)
						|| ((chproc[i].symbols_pream & 0x55) == 0x44)
						|| ((chproc[i].symbols_pream & 0xaa) == 0x88)
						|| ((chproc[i].symbols_pream & 0xaa) == 0x22))	//Preamble match
				{
					if(chproc[i].pream_match_cnt > CHPROC_PREAM_MATCH_N)	//Preamble is valid long enough
					{
						chproc[i].state = CHPROC_STATE_PREAM;	//Advance to syncword detection
						chproc[i].state_symbols = 0;
						chproc[i].symbols_sync = 0;
					}
					else
					{
						chproc[i].pream_match_cnt++;	//Count matches
					}
				}
				else	//Preamble not found
				{
					if(chproc[i].pream_match_cnt)	//Decrement match counter
					{
						chproc[i].pream_match_cnt--;
					}
					if(chproc[i].state_symbols > CHPROC_EXPIRE_MAG)	//State expired
					{
						chproc[i].state = CHPROC_STATE_IDLE;
						chproc_glob.atan2_cnt--;	//Return one atan2 to pool
#if GLOBAL_SEND_STUFF == 1
						if((chproc[i].channel == 63) && fdev_ptr && (fdev_ptr < FDEV_SIZE))
						{
							send = chproc[i].channel+1;
						}
#endif	/*GLOBAL_SEND_STUFF*/
					}
				}

#if GLOBAL_SEND_STUFF == 1
				if((chproc[i].channel == 63) && (send == 0) && (chproc[i].state_symbols == 1))
				{
					fdev[0] = chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] - chproc[i].dc_remove;
					data[0] = ((chproc[i].symbols_pream & 0x1) << 13) | (1 << 7);
					fdev_ptr = 1;
				}
				else if((chproc[i].channel == 63) && (send == 0) && (fdev_ptr && fdev_ptr < FDEV_SIZE))
				{
					fdev[fdev_ptr] = chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] - chproc[i].dc_remove;
					data[fdev_ptr] = ((chproc[i].symbols_pream & 0x1) << 13) | (1 << 7);
					fdev_ptr++;
					if(fdev_ptr == FDEV_SIZE)
					{
						send = chproc[i].channel+1;
					}
				}
#endif	/*GLOBAL_SEND_STUFF*/

				break;
			case CHPROC_STATE_PREAM:
				chproc_fdev(&chproc[i], fft_out);	//Get fdev
				chproc[i].symbols_sync <<= 1;	//Shift symbols in time
				if(chproc_wrap(((chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] + chproc[i].fdev[(chproc[i].packet_symbols-1) & CHPROC_FDEV_MASK]) >> 1)
													- chproc[i].dc_remove) > 0)	//Average of two symbols is larger than DC component
				{
					chproc[i].symbols_sync |= 0x1;	//Symbol '1'
				}
				if((chproc[i].symbols_sync & 0xaaaaaaaaaaaaaaaa) == (CHPROC_SYNCWORD & 0xaaaaaaaaaaaaaaaa))	//Syncword match
				{
#if 0	/*Complicated syncword match*/
					uint32_t match_pre_cnt;
					uint32_t match_post_cnt;
					uint32_t match_pre = ~((chproc[i].symbols_sync & 0x5555555555555554)
							^ ((CHPROC_SYNCWORD & 0x1555555555555555) << 2));	//XNOR with expected value before syncword match
					uint32_t match_post = ~((chproc[i].symbols_sync & 0x1555555555555555)
							^ (CHPROC_SYNCWORD & 0x1555555555555555));	//XNOR with expected value after syncword match

					for(match_pre_cnt = 0; match_pre; match_pre_cnt++)	//Count ones
					{
						match_pre &= (match_pre-1);	//Remove least significant 1
					}
					for(match_post_cnt = 0; match_post; match_post_cnt++)	//Count ones
					{
						match_post &= (match_post-1);	//Remove least significant 1
					}

					chproc[i].state = CHPROC_STATE_SYNC;
					if(match_pre_cnt > match_post_cnt)	//Better match before syncword, last symbol is already from data
					{
						chproc[i].state_symbols = 1;
						chproc[i].selected_after = 0;
					}
					else	//Better match after syncword
					{
						chproc[i].state_symbols = 0;
						chproc[i].selected_after = 1;
					}
#else	/*Complicated syncword match*/
					chproc[i].state = CHPROC_STATE_SYNC;	//Advance to data receiving
					chproc[i].state_symbols = 0;
					chproc[i].crc_cnt = 0;
					chproc[i].crc = CRC_CC1200_INIT_VALUE;
#endif	/*Complicated syncword match*/
				}
				else if(chproc[i].state_symbols > CHPROC_EXPIRE_PREAM)	//State expired
				{
					chproc[i].state = CHPROC_STATE_IDLE;
					chproc_glob.atan2_cnt--;	//Return one atan2 to pool
#if GLOBAL_SEND_STUFF == 1
					if((chproc[i].channel == 63) && fdev_ptr && (fdev_ptr < FDEV_SIZE))
					{
						send = chproc[i].channel+1;
					}
#endif	/*GLOBAL_SEND_STUFF*/
				}


#if GLOBAL_SEND_STUFF == 1
				if((chproc[i].channel == 63) && (send == 0) && (fdev_ptr && fdev_ptr < FDEV_SIZE))
				{
					fdev[fdev_ptr] = chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] - chproc[i].dc_remove;
					data[fdev_ptr] = ((chproc[i].symbols_sync & 0x1) << 12) | (1 << 6);
					fdev_ptr++;
					if(fdev_ptr == FDEV_SIZE)
					{
						send = chproc[i].channel+1;
					}
				}
#endif	/*GLOBAL_SEND_STUFF*/

				break;
			case CHPROC_STATE_SYNC:
				chproc_fdev(&chproc[i], fft_out);	//Get fdev
				if(chproc[i].state_symbols & 0x1)	//Each second symbol
				{
					chproc[i].byte <<= 1;	//Shift symbols in time
					if(chproc_wrap(((chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] + chproc[i].fdev[(chproc[i].packet_symbols-1) & CHPROC_FDEV_MASK]) >> 1)
														- chproc[i].dc_remove) > 0)	//Average of two symbols is larger than DC component
					{
						chproc[i].byte |= 0x1;	//Symbol '1'
					}
					if((chproc[i].state_symbols & 0xe) == 0xe)	//Byte complete
					{
						chproc[i].data[chproc[i].state_symbols >> 4] = chproc[i].byte;	//Store byte

						if((chproc[i].state_symbols >> 4)	//Length byte received
								&& ((chproc[i].state_symbols >> 4) == (chproc[i].data[0]+2)))	//Received 'length byte' bytes + 2 for CRC
						{
							chproc[i].state = CHPROC_STATE_CRC;	//Let main calculate CRC and receive the packet
							chproc_glob.atan2_cnt--;	//Return one atan2 to pool
#if GLOBAL_SEND_STUFF == 1
							if((chproc[i].channel == 63) && fdev_ptr && (fdev_ptr < FDEV_SIZE))
							{
								send = chproc[i].channel+1;
							}
#endif	/*GLOBAL_SEND_STUFF*/
						}
					}
				}
				if(chproc[i].state_symbols > CHPROC_EXPIRE_SYNC)
				{
					chproc[i].state = CHPROC_STATE_IDLE;	//Return to idle
					chproc_glob.atan2_cnt--;	//Return one atan2 to pool
#if GLOBAL_SEND_STUFF == 1
					if((chproc[i].channel == 63) && fdev_ptr && (fdev_ptr < FDEV_SIZE))
					{
						send = chproc[i].channel+1;
					}
#endif	/*GLOBAL_SEND_STUFF*/
				}

#if GLOBAL_SEND_STUFF == 1
				if((chproc[i].channel == 63) && (send == 0) && (fdev_ptr && fdev_ptr < FDEV_SIZE))
				{
					fdev[fdev_ptr] = chproc[i].fdev[chproc[i].packet_symbols & CHPROC_FDEV_MASK] - chproc[i].dc_remove;
					if(chproc[i].state_symbols & 0x1)
					{
						data[fdev_ptr] = ((chproc[i].byte & 0x1) << 13) | (1 << 7);
					}
					else
					{
						data[fdev_ptr] = 0;
					}
					fdev_ptr++;
					if(fdev_ptr == FDEV_SIZE)
					{
						send = chproc[i].channel+1;
					}
				}
#endif	/*GLOBAL_SEND_STUFF*/
				break;
			default:
				break;
			}

			chproc[i].state_symbols++;
			chproc[i].packet_symbols++;
		}
	}

	return active_cnt;	//Number of active chprocs
}

/**
 * @brief Call this function periodically to receive packets.
 * @return number of packets waiting to be received
 */
uint32_t chproc_pool()
{
	uint32_t i;
	uint32_t done = 0;

	for(i = 0; i < CHPROC_N; i++)
	{
		if(chproc[i].state == CHPROC_STATE_SYNC)	//Packet is being received
		{
			if(chproc[i].crc_cnt < (chproc[i].state_symbols >> 4))
			{
				chproc[i].crc = crc_CC1200_byte(chproc[i].crc, chproc[i].data[chproc[i].crc_cnt]);	//CRC one byte
				chproc[i].crc_cnt++;
			}
		}
		else if(chproc[i].state == CHPROC_STATE_CRC)	//Receiving done
		{
			for(; chproc[i].crc_cnt < (chproc[i].state_symbols >> 4); chproc[i].crc_cnt++)	//CRC the rest of the packet
			{
				chproc[i].crc = crc_CC1200_byte(chproc[i].crc, chproc[i].data[chproc[i].crc_cnt]);
			}
			if(chproc[i].crc)	//Bad packet
			{
				//chproc[i].state = CHPROC_STATE_IDLE;
				chproc[i].state = CHPROC_STATE_DONE;
				chproc[i].crcok = 0;
				done++;
			}
			else
			{
				chproc[i].state = CHPROC_STATE_DONE;
				chproc[i].crcok = 1;
				done++;
			}
		}
		else if(chproc[i].state == CHPROC_STATE_DONE)	//Packet CRCed
		{
			done++;
		}
	}

	return done;
}

