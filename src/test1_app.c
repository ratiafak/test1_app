/*
 ===============================================================================
 Name        : test1_app.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#ifdef __USE_CMSIS
#include "LPC43xx.h"
#endif

#include <string.h>

#include <cr_section_macros.h>
#include "r820t.h"
#include "i2c.h"
#include "adcdma.h"
#include "adc.h"
#include "uartsend.h"
#include "chproc.h"
#include "intercom.h"
#include "global_settings.h"

#define LED_R_INIT()    do{LPC_GPIO_PORT->DIR[0] |= (1 << 8);}while(0)
#define LED_R_ON()      do{LPC_GPIO_PORT->SET[0] = (1 << 8);}while(0)
#define LED_R_OFF()     do{LPC_GPIO_PORT->CLR[0] = (1 << 8);}while(0)

//#define M0_SLAVE_PAUSE_AT_MAIN

#if defined (M0_SLAVE_PAUSE_AT_MAIN)
volatile unsigned int pause_at_main;
#endif

intercom_t __attribute__ ((section(".noload.$RamAHB16"))) intercom;	///< Intercom structure at fixed memory location

typedef volatile struct
{
	uint32_t overruns_cnt;	///< Incremented each time when interrupt takes longer than one tick
#if GLOBAL_MEASURE_INTERRUPT_TIME > 0	/*Measure max and min*/
	uint32_t max;	///< Maximal time to process interrupt
	uint32_t min;	///< Minimal time to process interrupt
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
#if GLOBAL_MEASURE_INTERRUPT_TIME > 1	/*Store stats*/
	int16_t times[GLOBAL_MEASURE_INTERRUPT_TIME_MASK+1];	///< Store time statistics for interrupt
	int16_t atan2_cnt[GLOBAL_MEASURE_INTERRUPT_TIME_MASK+1];	///< Store number of calculated tangents
	uint32_t times_ptr;	///< Count stored times
	uint32_t send;	///< Set if sending is required
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
} interrupt_stats_t;

interrupt_stats_t interrupt_stats =
{
		.overruns_cnt = 0,
#if GLOBAL_MEASURE_INTERRUPT_TIME > 0	/*Measure max and min*/
		.max = 0,
		.min = UINT32_MAX,
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
#if GLOBAL_MEASURE_INTERRUPT_TIME > 1	/*Store stats*/
		.times_ptr = 0,
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
};

#define CYCLES_PER_TICK   (204000000/37500)	///< Number of processor cycles per one interrupt tick

r820t_priv_t priv;	///< Structure for r820t module


#define MAG_MIN_SHIFT          3	//Minimal magnitude increase in 2^n
#define MAG_MIN_ABS            4	//Minimal magnitude value

/**
 * @note The packet received should be:
 *
 * {20, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20}
 * data[1~4] = 0x206d0c30
 * data[5] = channel
 * data[6~7] = cnt
 *
 * {0x14, 0x30, 0x0c, 0x6d, 0x20, 0xcc, 0xnn, 0xnn, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14}
 */

void set_tuner();

int main(void)
{

#if defined (M0_SLAVE_PAUSE_AT_MAIN)
	// Pause execution until debugger attaches and modifies variable
	while(pause_at_main == 0);
#endif

	LED_R_INIT();	//Init red LED
	LPC_TIMER0->PR = 0;	//No prescaler
	LPC_TIMER0->TCR |= 0x1;	//Enable timer


	uint32_t lnagain = 0xb;
//	uint32_t lnagain = 0x9;
	uint32_t lastlnagain = 0;
//	uint32_t mixergain = 0x9;
	uint32_t mixergain = 0xb;
	uint32_t lastmixergain = 0;
	uint32_t vgagain = 0x7;
	uint32_t lastvgagain = 0;
	uint32_t freq = 865000000+2400000/2;	//Center frequency
	uint32_t lastfreq = 0;

	uartsend_init();	//Init uart transmit

	set_tuner();	//Set up the tuner
	//while(1)
	{
		if(lnagain != lastlnagain)
		{
			r820t_set_lna_gain(&priv, lnagain);
			lastlnagain = lnagain;
		}
		if(mixergain != lastmixergain)
		{
			r820t_set_mixer_gain(&priv, mixergain);
			lastmixergain = mixergain;
		}
		if(vgagain != lastvgagain)
		{
			r820t_set_vga_gain(&priv, vgagain);
			lastvgagain = vgagain;
		}
		if(freq != lastfreq)
		{
			r820t_set_freq(&priv, freq);
			lastfreq = freq;
		}
	}

	NVIC_EnableIRQ(M0_M4CORE_IRQn);	//Enable interrupt from M4
	adc_set_clock();	//Init ADC clock
	adc_init();	//Init ADC
	adcdma_init(intercom.buffer0, intercom.buffer1);	//Init DMA
	adc_start();	//Start ADC

	uint32_t i = 0;
	uint32_t led_on_time = 0;

	while(1)
	{
#if GLOBAL_SEND_STUFF == 1
		if(send)
		{
			uartsend_num_2array(send-1, fdev, data, fdev_ptr);	//Transfer copy
			send = 0;
		}
#endif	/*GLOBAL_SEND_STUFF*/

#if GLOBAL_SEND_STUFF == 3
		if(interrupt_stats.send)
		{
			uartsend_num_2array(0, interrupt_stats.times, interrupt_stats.atan2_cnt, GLOBAL_MEASURE_INTERRUPT_TIME_MASK+1);	//Transfer copy
			for(i = 1000000; i; i--);
			interrupt_stats.send = 0;
		}
#endif	/*GLOBAL_SEND_STUFF*/

		if(chproc_pool())	//CRC packets, true when there are complete packets
		{
			for(i = 0; i < CHPROC_N; i++)
			{
				if(chproc[i].state == CHPROC_STATE_DONE)	//Packet ready to be received
				{
					if(chproc[i].crcok)
					{
						LED_R_ON();
						led_on_time = LPC_TIMER0->TC;
					}
#if GLOBAL_SEND_STUFF == 2
#if GLOBAL_MEASURE_INTERRUPT_TIME > 1	/*Measure max*/
					uint32_t max = 0;
					uint32_t j;
					for(j = 0; j < GLOBAL_MEASURE_INTERRUPT_TIME_MASK; j++)
					{
						if(interrupt_stats.times[j] > interrupt_stats.times[max]) max = j;
					}
					uartsend_data_as_string((uint8_t*)&interrupt_stats.times[max], 2);
					uartsend_data_as_string((uint8_t*)&interrupt_stats.atan2_cnt[max], 1);
#elif GLOBAL_MEASURE_INTERRUPT_TIME > 0	/*Measure max*/
					uartsend_data_as_string((uint8_t*)&interrupt_stats.max, 2);
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
					uartsend_data_as_string((uint8_t*)&(chproc[i].channel), 1);
					if(chproc[i].crcok) uartsend_string("+\n\r  ");
					else uartsend_string("-\n\r  ");
					uartsend_data_as_string(chproc[i].data, chproc[i].data[0]+3);
					uartsend_string("\n\r");
#endif	/*GLOBAL_SEND_STUFF*/
					chproc[i].state = CHPROC_STATE_IDLE;
				}
			}
		}

		if((LPC_TIMER0->TC - led_on_time) > (50*204000))	//Time to turn off the LED
		{
			LED_R_OFF();
			led_on_time = LPC_TIMER0->TC;
		}
	}
	return 0;
}

/**
 * @brief Know how long the interrupt was executed.
 * @param cycles number of processor cycles for one interrupt
 */
void interrupt_time(uint32_t cycles)
{
	if(cycles > CYCLES_PER_TICK)
	{
		interrupt_stats.overruns_cnt++;
	}

#if GLOBAL_MEASURE_INTERRUPT_TIME > 0	/*Measure max and min*/
	if(cycles > interrupt_stats.max) interrupt_stats.max = cycles;
	if(cycles < interrupt_stats.min) interrupt_stats.min = cycles;
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/

#if GLOBAL_MEASURE_INTERRUPT_TIME > 1	/*Store stats*/
	if(interrupt_stats.send == 0)
	{
		interrupt_stats.times[interrupt_stats.times_ptr] = cycles;
		interrupt_stats.atan2_cnt[interrupt_stats.times_ptr] = chproc_glob.atan2_cnt << 8;
		interrupt_stats.times_ptr++;
		if(interrupt_stats.times_ptr > GLOBAL_MEASURE_INTERRUPT_TIME_MASK)
		{
			interrupt_stats.times_ptr = 0;
			interrupt_stats.send = 1;
		}
	}
#endif	/*GLOBAL_MEASURE_INTERRUPT_TIME*/
}

/**
 * @brief Interrupt handler from M4.
 */
void M0_M4CORE_IRQHandler()
{
	uint32_t i;
	uint32_t interrupt_start;

	LPC_CREG->M4TXEVENT = 0;	//Clear the interrupt

	interrupt_start = LPC_TIMER0->TC;	//Mark beginning of the interrupt

	for(i = intercom.mag_channels; i < (intercom.mag_channels+32); i += 4)	//Check calculated magnitudes, unrolled by 4
	{
#warning "Increase minimum above 0x1f?"
		if((intercom.mag[i] > (intercom.mag_last[i] << MAG_MIN_SHIFT))	//N times increase
				&& (intercom.mag[i] > MAG_MIN_ABS))	//Over the minimum threshold
		{
			chproc_impulse_power(i);	//Power condition on channel i
		}
		if((intercom.mag[i+1] > (intercom.mag_last[i+1] << MAG_MIN_SHIFT))	//N times increase
				&& (intercom.mag[i+1] > MAG_MIN_ABS))	//Over the minimum threshold
		{
			chproc_impulse_power(i+1);	//Power condition on channel i
		}
		if((intercom.mag[i+2] > (intercom.mag_last[i+2] << MAG_MIN_SHIFT))	//N times increase
				&& (intercom.mag[i+2] > MAG_MIN_ABS))	//Over the minimum threshold
		{
			chproc_impulse_power(i+2);	//Power condition on channel i
		}
		if((intercom.mag[i+3] > (intercom.mag_last[i+3] << MAG_MIN_SHIFT))	//N times increase
				&& (intercom.mag[i+3] > MAG_MIN_ABS))	//Over the minimum threshold
		{
			chproc_impulse_power(i+3);	//Power condition on channel i
		}
	}

	chproc_tick(intercom.fft_out);	//Process one sample of active channels

	interrupt_time(LPC_TIMER0->TC - interrupt_start);	//Mark how long the interrupt took
}

void check_failed(uint8_t* file, uint32_t line)
{
	__asm("BKPT");
}

/* Configuration for R820T2 initial values */
/* Those initial values start from REG_SHADOW_START */
/* Shall be not set as const as the structure is used as IN & OUT */
r820t_priv_t priv = {
  16000000,	// xtal_freq => 16MHz
  6000000,	// if_freq
	100000000,	// freq
  {
    /* 05 */ 0x90, // LNA manual gain mode, init to 0
    /* 06 */ 0x80,
    /* 07 */ 0x60,
    /* 08 */ 0x80, // Image Gain Adjustment
    /* 09 */ 0x40, // Image Phase Adjustment
    /* 0A */ 0xA8, // Channel filter [0..3]: 0 = widest, f = narrowest - Optimal. Don't touch!
    /* 0B */ 0x0F, // High pass filter - Optimal. Don't touch!
    /* 0C */ 0x40, // VGA control by code, init at 0
    /* 0D */ 0x63, // LNA AGC settings: [0..3]: Lower threshold; [4..7]: High threshold
    /* 0E */ 0x75,
    /* 0F */ 0xF8, // Clk out off
    /* 10 */ 0x7C,
    /* 11 */ 0x83,
    /* 12 */ 0x80,
    /* 13 */ 0x00,
    /* 14 */ 0x0F,
    /* 15 */ 0x00,
    /* 16 */ 0xC0,
    /* 17 */ 0x30,
    /* 18 */ 0x48,
    /* 19 */ 0xCC,
    /* 1A */ 0x60,
    /* 1B */ 0x00,
    /* 1C */ 0x54,
    /* 1D */ 0xAE,
    /* 1E */ 0x0A,
    /* 1F */ 0xC0
  },
  0 /* uint16_t padding */
};

/**
 * @brief Set up the tuner
 */
void set_tuner()
{
	i2c_init(LPC_I2C0);	//Init I2C

	r820t_init(&priv, 2400000/2);	//IF frequency
	r820t_set_if_bandwidth(&priv, 59);	//IF bandwidth

#if 0
	r820t_set_lna_agc(&priv, 1);	//Auto gain on LNA
	r820t_set_mixer_agc(&priv, 1);	//Auto gain on mixer
#else
	r820t_set_lna_agc(&priv, 0);	//Manual gain on LNA
	r820t_set_mixer_agc(&priv, 0);	//Manual gain on mixer
#endif
}

