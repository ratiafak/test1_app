/*
 * i2c.c
 *
 *  Created on: Apr 5, 2018
 *      Author: Tomas Jakubik
 *    I2C wrapper for librtlsdr.
 */

#include "driverlib_lpc43xx_i2c.h"	//I2C driver with removed dependencies
#include "i2c.h"
#include "intercom.h"	//To get SystemCoreClock

/**
 * @brief Initialize I2C.
 */
void i2c_init(void* dev)
{
	I2C_Init(dev, 100000, *intercom.ptr_SystemCoreClock);
	I2C_Cmd(dev, ENABLE);
}

/**
 * @brief Write data to I2C
 * @param dev I2C device
 * @param addr I2C address aligned to MSb
 * @param buf data buffer
 * @param len length in bytes
 * @return number of bytes transmitted
 */
int i2c_write_fn(void *dev, uint8_t addr, uint8_t *buf, int len)
{
	volatile Status ret;
	I2C_M_SETUP_Type i2csetup;

	i2csetup.callback = NULL;
	i2csetup.retransmissions_max = 2;
	i2csetup.retransmissions_count = 0;
	i2csetup.rx_count = 0;
	i2csetup.rx_data = NULL;
	i2csetup.rx_length = 0;
	i2csetup.sl_addr7bit = addr >> 1;	//7 bit address aligned to the LSb
	i2csetup.status = 0;
	i2csetup.tx_count = 0;
	i2csetup.tx_data = buf;
	i2csetup.tx_length = len;

	ret = I2C_MasterTransferData(dev, &i2csetup, I2C_TRANSFER_POLLING);	//Send

	if(ret != SUCCESS) return -1;	//Return error
	return i2csetup.tx_count;	//Return number of sent bytes
}

/**
 * @brief Read data from I2C
 * @param dev I2C device
 * @param addr I2C address aligned to MSb
 * @param buf data buffer
 * @param len length in bytes
 * @return number of bytes read
 */
int i2c_read_fn(void *dev, uint8_t addr, uint8_t *buf, int len)
{
	volatile Status ret;
	I2C_M_SETUP_Type i2csetup;

	i2csetup.callback = NULL;
	i2csetup.retransmissions_max = 2;
	i2csetup.retransmissions_count = 0;
	i2csetup.rx_count = 0;
	i2csetup.rx_data = buf;
	i2csetup.rx_length = len;
	i2csetup.sl_addr7bit = addr >> 1;	//7 bit address aligned to the LSb
	i2csetup.status = 0;
	i2csetup.tx_count = 0;
	i2csetup.tx_data = NULL;
	i2csetup.tx_length = 0;

	ret = I2C_MasterTransferData(dev, &i2csetup, I2C_TRANSFER_POLLING);	//Send

	if(ret != SUCCESS) return -1;	//Return error
	return i2csetup.rx_count;	//Return number of received bytes
}



