/*
 * delay.c
 *
 *  Created on: Apr 19, 2018
 *      Author: Tomas Jakubik
 */

#include "delay.h"

/**
 * @brief MCU delay.
 * @param time time [us]
 */
void delay_us(uint32_t time)
{
	volatile uint32_t delay;

	while(time--)	//Wait for a moment
	{
		for(delay = 15; delay; delay--);
	}
}

/**
 * @brief Replacement for system call.
 * @param from time [us]
 * @param to time [us]
 */
void usleep_range(uint32_t from, uint32_t to)
{
	uint32_t time = (from/2) + (to/2);
	delay_us(time);
}
