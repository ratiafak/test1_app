/*
 * cordic_atan2.c
 *
 *  Created on: November 2017
 *      Author: ST microelectronics DT0087
 *  Modified on: Aug 24, 2018
 *      Author: Tomas Jakubik
 *
 *  This module calculates atan2 using CORDIC algorithm.
 */

#include "cordic_atan2.h"

const int CORDIC_ZTBL[] = {
		0x00001922, 0x00000ED6, 0x000007D7, 0x000003FB, 0x000001FF, 0x00000100, 0x00000080, 0x00000040,
		0x00000020, 0x00000010, 0x00000008, 0x00000004, 0x00000002, 0x00000001
};

#if 0	/*Orignal form*/
// angle is radians multiplied by CORDIC multiplication factor M
// modulus can be set to CORDIC inverse gain 1/F to avoid post-division
void CORDICatan2sqrt(int *a, int *m, int y, int x)
{
	int k, tx, z = 0, fl = 0;
	if(x < 0)
	{
		fl = ((y > 0) ? +1 : -1);
		x = -x;
		y = -y;
	}
	for(k = 0; k < CORDIC_MAXITER; k++)
	{
		tx = x;
		if(y <= 0)
		{
			x -= (y >> k);
			y += (tx >> k);
			z -= CORDIC_ZTBL[k];
		}
		else
		{
			x += (y >> k);
			y -= (tx >> k);
			z += CORDIC_ZTBL[k];
		}
	}

	if(fl != 0)
	{
		z += fl * CORDIC_PI;
	}
	*a = z; // radians multiplied by factor M
	*m = x; // sqrt(x^2+y^2) multiplied by gain F
}
#endif	/*Orignal form*/

/**
 * @brief Calculate atan2.
 * @param y
 * @param x
 * @return angle radians multiplied by CORDIC multiplication factor M
 */
int cordic_atan2(int y, int x)
{
	int k, tx, z = 0, fl = 0;
	if(x < 0)
	{
		fl = ((y > 0) ? +1 : -1);
		x = -x;
		y = -y;
	}
	for(k = 0; k < CORDIC_MAXITER; k++)
	{
		tx = x;
		if(y <= 0)
		{
			x -= (y >> k);
			y += (tx >> k);
			z -= CORDIC_ZTBL[k];
		}
		else
		{
			x += (y >> k);
			y -= (tx >> k);
			z += CORDIC_ZTBL[k];
		}
	}

	if(fl != 0)
	{
		z += fl * CORDIC_PI;
	}
	return z; // radians multiplied by factor M
}
