/*
 * uartsend.c
 *
 *  Created on: Jun 22, 2018
 *      Author: Tomas Jakubik
 */

#include "uartsend.h"

/**
 * @brief Array for conversion from hex nibble to ascii character
 */
char hex2char[16] = "0123456789abcdef";

/**
 * @brief Init this module.
 */
void uartsend_init()
{
	LPC_CGU->BASE_UART2_CLK = (0x9 << 24) | (1 << 11);	//Enable clock
	LPC_CCU1->CLK_M4_USART2_CFG = 0x1;
	LPC_CCU2->CLK_APB2_USART2_CFG = 0x1;

	LPC_SCU->SFSP2_10 = (1 << 4) | 0x2;	//Disable pull-up, USART2 TXD

	LPC_USART2->FCR = 0x7;	//Enable and reset FIFOs
	LPC_USART2->LCR = (1 << 7) | (1 << 2) | 0x3;	//No parity, two stop bits, 8 bit frame, enable access to divisor latches
	LPC_USART2->DLL = 110;	//204 MHZ / (110*16) = 115909 Baud/s = 1.006155303*115200 Baud/s
	LPC_USART2->DLM = 0;
	LPC_USART2->LCR = (1 << 2) | 0x3;	//Disable access to divisor latches

	LPC_USART2->TER = 0x1;	//Enable transmit
}

#if 0
/**
 * @brief Send "Nasrat na hrad!".
 */
void uartsend_nasratnahrad()
{
	char nasratnahrad[] = "Nasrat na hrad!";
	uint8_t* ptr;
	ptr = (uint8_t*)nasratnahrad;

	while(*ptr)
	{
		uartsend_byte(*ptr);
		ptr++;
	}
}
#endif /*0*/

/**
 * @brief Send string.
 * @param string
 */
void uartsend_string(char* string)
{
	while(*string)
	{
		uartsend_byte(*string);
		string++;
	}
}

/**
 * @brief Send data as "0xNN, 0xNN" string
 * @param data byte data
 * @param length number of bytes
 */
void uartsend_data_as_string(uint8_t* data, uint32_t length)
{
	uint32_t i;
	for(i = 0; i < length; i++)
	{
		uartsend_byte(hex2char[((data[i] >> 4) & 0xf)]);
		uartsend_byte(hex2char[(data[i] & 0xf)]);
		uartsend_byte(',');
	}
}

/**
 * @brief Send array of int16_t data.
 * @param array data to send
 * @param size number of elements in array
 */
void uartsend_array(int16_t* array, size_t size)
{
	uint32_t i;
	uartsend_byte(0x80);	//Send start of frame
	uartsend_byte(0x80);

	for(i = 0; i < size; i++)
	{
		uartsend_byte(array[i] & 0xff);	//Send data
		uartsend_byte((array[i] >> 8) & 0xff);
	}

	uartsend_byte(0x81);	//Send end of frame
	uartsend_byte(0x81);
}

/**
 * @brief Send number and array of int16_t data.
 * @param number number send before the array
 * @param array data to send
 * @param size number of elements in array
 */
void uartsend_num_array(int16_t number, int16_t* array, size_t size)
{
	uint32_t i;
	uartsend_byte(0x80);	//Send start of frame
	uartsend_byte(0x80);

	uartsend_byte(number);	//Send number
	uartsend_byte(number >> 8);

	for(i = 0; i < size; i++)
	{
		uartsend_byte(array[i] & 0xff);	//Send data
		uartsend_byte((array[i] >> 8) & 0xff);
	}

	uartsend_byte(0x81);	//Send end of frame
	uartsend_byte(0x81);
}

/**
 * @brief Send array of int16_t data.
 * @param array1 data to send
 * @param array2 data to send
 * @param size number of elements in arrays
 */
void uartsend_2array(int16_t* array1, int16_t* array2, size_t size)
{
	uint32_t i;
	uartsend_byte(0x80);	//Send start of frame
	uartsend_byte(0x80);

	for(i = 0; i < size; i++)
	{
		uartsend_byte(array1[i] & 0xff);	//Send data 1
		uartsend_byte((array1[i] >> 8) & 0xff);
		uartsend_byte(array2[i] & 0xff);	//Send data 2
		uartsend_byte((array2[i] >> 8) & 0xff);
	}

	uartsend_byte(0x81);	//Send end of frame
	uartsend_byte(0x81);
}

/**
 * @brief Send array of int16_t data.
 * @param number number send before the array
 * @param array1 data to send
 * @param array2 data to send
 * @param size number of elements in arrays
 */
void uartsend_num_2array(int16_t number, int16_t* array1, int16_t* array2, size_t size)
{
	uint32_t i;
	uartsend_byte(0x80);	//Send start of frame
	uartsend_byte(0x80);

	uartsend_byte(number);	//Send number
	uartsend_byte(number >> 8);

	for(i = 0; i < size; i++)
	{
		uartsend_byte(array1[i] & 0xff);	//Send data 1
		uartsend_byte((array1[i] >> 8) & 0xff);
		uartsend_byte(array2[i] & 0xff);	//Send data 2
		uartsend_byte((array2[i] >> 8) & 0xff);
	}

	uartsend_byte(0x81);	//Send end of frame
	uartsend_byte(0x81);
}

/**
 * @brief Send one byte while checking FIFO.
 * @param byte byte to send
 */
void uartsend_byte(uint8_t byte)
{
	static uint32_t fifo_counter = 0;

	if(LPC_USART2->LSR & (1 << 5))	//Transmitter holding register empty
	{
		fifo_counter = 14;	//Set counter for 14 more bytes
	}
	else if(fifo_counter > 0)	//Some space in FIFO from last time
	{
		fifo_counter--;
	}
	else	//FIFO might be full
	{
		while(!(LPC_USART2->LSR & (1 << 5)));	//Wait for transmitter holding register empty
	}

	LPC_USART2->THR = byte;	//Send
}
